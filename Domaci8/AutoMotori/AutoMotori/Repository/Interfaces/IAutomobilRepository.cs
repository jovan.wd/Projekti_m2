﻿using AutoMotori.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoMotori.Repository.Interfaces
{
    public interface IAutomobilRepository
    {
        IEnumerable<Automobil> GetAll();
        Automobil GetById(int id);
        bool Create(Automobil automobil);
        void Update(Automobil automobil);
        void Delete(int id);
    }
}
