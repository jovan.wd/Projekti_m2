﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AutoMotori.Models
{
    public class Automobil
    {
        public int Id { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 3)]
        [RegularExpression("[A-Z][A-Za-z ]*")]
        public string Proizvodjac { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 3)]
        [RegularExpression("[A-Z][A-Za-z ]*")]
        public string Model { get; set; }

        [Range(1990, 2017)]
        public int Godiste { get; set; }

        [Range(800, 2200)]
        public int Zapremina { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 3)]
        [RegularExpression("[A-Z][A-Za-z ]*")]
        public string Boja { get; set; }

        public TipMotora TipMotora { get; set; }

    }
}