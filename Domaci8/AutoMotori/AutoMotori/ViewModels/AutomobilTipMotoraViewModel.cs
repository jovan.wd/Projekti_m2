﻿using AutoMotori.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoMotori.ViewModels
{
    public class AutomobilTipMotoraViewModel
    {
        public Automobil Automobil { get; set; }
        public IEnumerable<TipMotora> TipoviMotora { get; set; }
        public int SelectedIdTipMotora { get; set; }

    }
}