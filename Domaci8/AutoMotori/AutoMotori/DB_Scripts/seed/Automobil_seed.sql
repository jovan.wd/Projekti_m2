﻿delete from Automobil

DBCC CHECKIDENT ('Automobil', RESEED, 0)
GO

insert into Automobil(AutomobilProizvodjac, AutomobilModel,AutomobilGodisete,AutomobilZapremina,AutomobilBoja,AutomobilTipMotoraId)
values
	('Mercedes', 'A170', 2000, 1700, 'crna', 2),
	('Bmw', 'e320', 2001, 2000, 'plava', 1),
	('Ford', 'Focus', 2003, 1800, 'siva', 2),
	('Alfa Romeo', '147', 2005, 1600, 'zelena', 3),
	('Tesla', 'Tmobil', 2016, 1000, 'crvena', 4)