﻿using AutoMotori.Models;
using AutoMotori.Repository;
using AutoMotori.Repository.Interfaces;
using AutoMotori.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AutoMotori.Controllers
{
    public class AutomobilController : Controller
    {
        // GET: Automobil
        private IAutomobilRepository automobilRepo = new AutomobilRepository();
        private ITipMotoraRepository tipMotoraRepo = new TipMotoraRepository();
        // GET: Product
        public ActionResult Index()
        {
            var automobili = automobilRepo.GetAll();

            return View(automobili);
        }

        // GET: Product/Details/5
        public ActionResult Details(int id)
        {
            var automobil = automobilRepo.GetById(id);
            return View(automobil);
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            var tipoviMotora = tipMotoraRepo.GetAll();

            var tmViewModel = new AutomobilTipMotoraViewModel
            {
                TipoviMotora = tipoviMotora.ToList()
            };

            return View(tmViewModel);
        }

        // POST: Automobil/Create
        [HttpPost]
        public ActionResult Create(Automobil automobil)
        {
            if (!ModelState.IsValid) 
            {
                return View(automobil);
            }

            automobilRepo.Create(automobil);
            return RedirectToAction("Index");
        }

        // GET: Automobil/Edit/5
        public ActionResult Edit(int id)
        {
            var automobil = automobilRepo.GetById(id);
            var tipoviMotora = tipMotoraRepo.GetAll();

            var tmViewModel = new AutomobilTipMotoraViewModel
            {
                Automobil = automobil,
                TipoviMotora = tipoviMotora.ToList()
            };

            return View(tmViewModel);
        }

        // POST: Automobil/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            if (ModelState.IsValid)
            {
                string autoProizvodjac = collection["Automobil.Proizvodjac"];
                string autoModel = collection["Automobil.Model"];
                int autoGodiste = int.Parse(collection["Automobil.Godiste"]);
                int autoZapremina = int.Parse(collection["Automobil.Zapremina"]);
                string autoBoja = collection["Automobil.Boja"];
                int tipMotoraId = int.Parse(collection["Automobil.TipMotora.Id"]);

                var updateAutomobil = new Automobil() {Id=id, Proizvodjac = autoProizvodjac, Model = autoModel, Godiste = autoGodiste, Zapremina = autoZapremina, Boja = autoBoja, TipMotora = tipMotoraRepo.GetById(tipMotoraId) };
                
                automobilRepo.Update(updateAutomobil);

                return RedirectToAction("Index");
            }
            return View(new Automobil());

        }

        // GET: Automobil/Delete/5
        public ActionResult Delete(int id)
        {
            automobilRepo.Delete(id);
            return RedirectToAction("Index");
        }
    }
}