﻿window.onload = function () {
    var proizvodjac = document.querySelector("input[name='Name']");
    var price = document.querySelector("input[name='Price']");

    name.onfocus = function () {
        let nameSpan = document.querySelector("#nameSpan")
        nameSpan.innerText = ""
    }

    price.onfocus = function () {
        let priceSpan = document.querySelector("#priceSpan")
        priceSpan.innerText = ""
    }
}

function posalji(form) {
    var proizvodjac = form["Automobil.Proizvodjac"].value;
    var model = form["Automobil.Model"].value;
    var godiste = form["Automobil.Godiste"].value;
    var zapremina = form["Automobil.Zapremina"].value;
    var boja = form["Automobil.Boja"].value;

    if (!proizvodjac) {
        let proizvodjacSpan = document.querySelector("#proizvodjacSpan")
        proizvodjacSpan.innerText = "polje ne sme biti prazno"
    }
    if (!model) {
        let modelSpan = document.querySelector("#modelSpan")
        modelSpan.innerText = "polje ne sme biti prazno"
    }
    if (!godiste) {
        let godisteSpan = document.querySelector("#godisteSpan")
        godisteSpan.innerText = "polje ne sme biti prazno"
    }
    if (!zapremina) {
        let zapreminaSpan = document.querySelector("#zapreminaSpan")
        zapreminaSpan.innerText = "polje ne sme biti prazno"
    }
    if (!boja) {
        let bojaSpan = document.querySelector("#bojaSpan")
        bojaSpan.innerText = "polje ne sme biti prazno"
    }
    if (godiste < 1990 && godiste >2018) {
        let godisteSpan = document.querySelector("#godisteSpan")
        godisteSpan.innerText = "godiste mora biti izmedju 1990-2018"
    }
    if (zapremina < 900 && godiste > 3000) {
        let zapreminaSpan = document.querySelector("#zapreminaSpan")
        zapreminaSpan.innerText = "kubikaza mora biti izmedju 900 - 3000 ccm3"
    }

    console.log(`Naziv proizvoda je ${name}, a cena je ${price}`);
    return false;
}