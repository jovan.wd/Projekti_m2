﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMotori.Repository.Interfaces;
using AutoMotori.Repository;
using AutoMotori.Models;
using AutoMotori.Controllers;
using AutoMotori.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AutoMotori.Tests
{
    class AutomobilUnitTest1
    {
        private IAutomobilRepository automobilRepo = new AutomobilRepository();
        private ITipMotoraRepository tipMotoraRepo = new TipMotoraRepository();

        [TestMethod]
        public void CreateAutomobilTest()
        {
            Automobil a = new Automobil();

            a.Proizvodjac = "TestProizvodjac";
            a.Model = "TestModel";
            a.Godiste = 2007;
            a.Zapremina = 1999;
            a.Boja = "crvena";
            a.TipMotora.Id = 2;

            Assert.IsTrue(automobilRepo.Create(a));

        }
    }
}
