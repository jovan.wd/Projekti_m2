﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookStore.Controllers
{
    public class BookStoreController : Controller
    {
        // GET: BookStore
        public ActionResult Index()
        {
            ViewModels.BookGenreViewModel genreViewModel = new ViewModels.BookGenreViewModel();
            genreViewModel.Genres = BookStore.Models.Books.Zanrovi;
            return View(genreViewModel);
        }

        [HttpPost]
        public ActionResult Index(ViewModels.BookGenreViewModel vm)
        {
            int lastID = Models.Books.Knjige.Last().Id;
            Models.BookStore knjizara = new Models.BookStore();

            Models.Book knjiga = vm.Book;
            Models.Genre genre;
            foreach (var gen in Models.Books.Zanrovi)
            {
                if (gen.Id == vm.SelectedGenreId)
                {
                    genre = gen;
                    knjiga.Genre = genre;
                }
            }
            knjiga.Id = lastID+1;
            Models.Books.Knjige.Add(knjiga);

            return RedirectToAction("List");
        }
    

        [HttpGet]
        public ActionResult List()
        {
            return View(Models.Books.Knjige);
        }
        
        [HttpGet]
        public ActionResult Deleted()
        {
            return View(Models.Books.Knjige);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            Models.Book nadjenaKnjiga = null;
            ViewModels.BookGenreViewModel vm = new ViewModels.BookGenreViewModel();
            foreach (var item in Models.Books.Knjige)
            {
                if (item.Id == id)
                {
                    nadjenaKnjiga = item;
                }
            }

            vm.Book = nadjenaKnjiga;
            vm.Genres = Models.Books.Zanrovi;

            return View(vm);
        }
        [HttpPost]
        public ActionResult Edit(ViewModels.BookGenreViewModel vm)
        {
            Models.Genre newGenre = null;
            foreach (var genre in Models.Books.Zanrovi)
            {
                if (genre.Id == vm.SelectedGenreId)
                {
                    newGenre = genre;
                }
            }
            foreach (var book in Models.Books.Knjige)
            {
                if (book.Id == vm.Book.Id)
                {
                    book.Name = vm.Book.Name;
                    book.Price = vm.Book.Price;
                    book.Genre = newGenre;
                }
            }
            return RedirectToAction("List");
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            foreach (Models.Book b in Models.Books.Knjige)
            {
                if (b.Id == id)
                    b.Deleted = true;
            }
            return RedirectToAction("List");
            
        }
        
        [HttpGet]
        public ActionResult Search()
        {
            return View(Models.Books.Knjige);
        }

        [HttpPost]
        public ActionResult Search(string Kriterijum, string Naziv)
        {
            List<Models.Book> list = new List<Models.Book>();
            if (Naziv == "nazivu knjige")
            {
                foreach (Models.Book b in Models.Books.Knjige)
                {
                    if (b.Name.Contains(Kriterijum))
                    {
                        Models.Book a = new Models.Book();
                        a.Id = b.Id;
                        a.Name= b.Name;
                        a.Price = b.Price;
                        a.Genre = b.Genre;
                        a.Chapter= b.Chapter;
                        list.Add(a);
                    }
                }
            }
            else if (Naziv == "nazivu poglavlja")
            {
                foreach (Models.Book b in Models.Books.Knjige)
                {
                    if (b.Chapter.Name.Contains(Kriterijum))
                    {
                        list.Add(b);
                    }
                }
            }
                
            return View(list);
        }

        public ActionResult SortName()
        {
            Models.Books.Knjige = Models.Books.Knjige.OrderBy(x => x.Name).ToList();
            return RedirectToAction("List");
        }
        public ActionResult SortPrice()
        {
            Models.Books.Knjige = Models.Books.Knjige.OrderBy(x => x.Price).ToList();
            return RedirectToAction("List");
        }
        public ActionResult SortGenre()
        {
            Models.Books.Knjige = Models.Books.Knjige.OrderBy(x => x.Genre.Name).ToList();
            return RedirectToAction("List");
        }
    }
}