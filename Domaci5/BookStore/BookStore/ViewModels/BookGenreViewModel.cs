﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.ViewModels
{
    public class BookGenreViewModel
    {
        public Models.Book Book { get; set; }
        public Models.Genre Genre { get; set; }
        public List<Models.Genre> Genres { get; set; }
        public int SelectedGenreId { get; set; }

        public BookGenreViewModel()
        {
            Book = null;
            Genre = null;
            Genres = new List<Models.Genre>();
        }
    }
}