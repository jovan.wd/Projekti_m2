﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace BookStore.Models
{
    public class Books
    {
        public static List<Genre> Zanrovi = new List<Genre>()
        {
            new Genre() {Name="Science"},
            new Genre() { Name = "Comedy"},
            new Genre() { Name = "Horror"}
        };

        public static List<Chapter> Poglavlja = new List<Chapter>()
        {
            new Chapter(){Name = "Dvorana tajni"},
            new Chapter(){Name = "Poglavlje1"},
            new Chapter(){Name = "Stari mlin"},
            new Chapter(){Name = "Under the Sea"}
        };

        public static List<Book> Knjige = new List<Book>()
        {
            new Book(){Name = "Harry Potter", Price=230, Genre=Zanrovi[0], Chapter= Poglavlja[0]},
            new Book(){Name = "Druzina Pere Kvrzice", Price=199, Genre=Zanrovi[1], Chapter= Poglavlja[2]},
            new Book(){Name = "Tvrdjava", Price=550, Genre=Zanrovi[2], Chapter= Poglavlja[1]},
            new Book(){Name = "20.000  Leagues Under the Sea", Price=1990, Genre=Zanrovi[0], Chapter= Poglavlja[1]}
        };
    }
}
