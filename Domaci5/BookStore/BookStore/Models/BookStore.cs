﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Models
{
    public class BookStore
    {
        private static int brojac = 1;

        public int Id { get; set; }
        public string  Name { get; set; }
        public List<Book> Lista { get; set; }

        public BookStore()
        {
            Id = brojac++;
            Name = "";
            Lista = new List<Book>();
        }
    }
}