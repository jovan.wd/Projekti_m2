﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Models
{
    public class Book
    {
        private static int brojac = 1;

        public int Id { get; set; }
        public string  Name { get; set; }
        public double Price { get; set; }
        public Genre Genre { get; set; }
        public bool Deleted { get; set; }
        public Chapter Chapter { get; set; }

        public Book()
        {
            Id = brojac++;
            Name = "";
            Price = 0;
            Genre = null;
            Deleted = false;
            Chapter = null;
        }
        
    }
}