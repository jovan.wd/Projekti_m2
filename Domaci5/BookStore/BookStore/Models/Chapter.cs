﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Models
{
    public class Chapter
    {
        private static int brojac=1;
        public int Id { get; set; }
        public string Name { get; set; }

        public Chapter()
        {
            Id = brojac++;
            Name = "";
        }
    }
}