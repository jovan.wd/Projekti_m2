﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookStore.Controllers
{
    public class BookStoreController : Controller
    {
        public static Models.Books books = new Models.Books(@"~/App_Data/books.txt");
        public static Dictionary<int, Models.Book> recnik = books.list;
        // GET: BookStore
        public ActionResult Index()
        {
            return View();
        }
        

        [HttpGet]
        public ActionResult List()
        {
            return View(recnik);
        }
        
        [HttpGet]
        public ActionResult Deleted()
        {
            return View(recnik);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            foreach (KeyValuePair<int, Models.Book> b in recnik)
            {
                if (b.Value.Id == id)
                    b.Value.Deleted = true;
            }
            return RedirectToAction("List");
            
        }
        
        [HttpPost]
        public ActionResult Add(string Name, double Price, string Genre)
        {
            int lastID = recnik.Last().Value.Id + 1;
            Models.BookStore knjizara = new Models.BookStore();
            
            Models.Book knjiga = new Models.Book();
            knjiga.Name = Name;
            knjiga.Price = Price;
            knjiga.Genre = Genre;

            recnik.Add(lastID, knjiga);
    
            knjizara.Name = "Moja knjizara";
            knjizara.Lista = recnik;
            return RedirectToAction("List");
        }

        List<Models.Book> lista = new List<Models.Book>();
        [HttpGet]
        public ActionResult Search()
        {
            return View(lista);
        }

        [HttpPost]
        public ActionResult Search(string Kriterijum, string Naziv)
        {
            if (Naziv == "nazivu knjige")
            {
                foreach (KeyValuePair<int, Models.Book> b in recnik)
                {
                    if (b.Value.Name.Contains(Kriterijum))
                    {
                        BookStore.Models.Book a = new Models.Book();
                        a.Id = b.Value.Id;
                        a.Name= b.Value.Name;
                        a.Price = b.Value.Price;
                        a.Genre = b.Value.Genre;
                        a.Chapter.Id = b.Value.Chapter.Id;
                        a.Chapter.lista = b.Value.Chapter.lista;
                        lista.Add(a);
                    }
                }
            }
            else if (Naziv == "nazivu poglavlja")
            {
                foreach (KeyValuePair<int, Models.Book> b in recnik)
                {
                    foreach (string c in b.Value.Chapter.lista)
                    {
                        if(c.Contains(Kriterijum))
                            lista.Add(b.Value);
                    }
                }
            }
                
            return View(lista);
        }

        public ActionResult SortName()
        {
            recnik = recnik.OrderBy(x => x.Value.Name).ToDictionary(x => x.Key, y => y.Value);
            return RedirectToAction("List");
        }
        public ActionResult SortPrice()
        {
            recnik = recnik.OrderBy(x => x.Value.Price).ToDictionary(x => x.Key, y => y.Value);
            return RedirectToAction("List");
        }
        public ActionResult SortGenre()
        {
            recnik = recnik.OrderBy(x => x.Value.Genre).ToDictionary(x => x.Key, y => y.Value);
            return RedirectToAction("List");
        }
    }
}