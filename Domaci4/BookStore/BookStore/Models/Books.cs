﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace BookStore.Models
{
    public class Books
    {
        public Dictionary<int, Book> list;

        public Books(string path)
        {
            path = HostingEnvironment.MapPath(path);
            list = new Dictionary<int, Book>();
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split(',');
                Book b = new Book();
                b.Name = tokens[0];
                b.Price = double.Parse(tokens[1]);
                b.Genre = tokens[2];
                b.Chapter = new Chapter();
                b.Chapter.lista.Add(tokens[3]);
                b.Chapter.lista.Add(tokens[4]);
                b.Chapter.lista.Add(tokens[5]);
                list.Add(b.Id,b);
            }
            sr.Close();
            stream.Close();
        }
    }
}
