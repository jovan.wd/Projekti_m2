﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Models
{
    public class Chapter
    {
        private static int brojac=1;
        public int Id { get; set; }
        public List<string> lista;

        public Chapter()
        {
            Id = brojac++;
            lista = new List<string>();
        }
    }
}