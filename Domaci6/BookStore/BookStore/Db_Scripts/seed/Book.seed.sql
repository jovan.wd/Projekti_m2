﻿delete from Book

DBCC CHECKIDENT ('Book', RESEED, 0)
GO

INSERT INTO dbo.Book (BookName, BookPrice, BookGenre, BookDeleted) VALUES ('Harry Potter', 550, 1, 'false')
INSERT INTO dbo.Book (BookName, BookPrice, BookGenre, BookDeleted) VALUES ('Tvrdjava', 299, 3, 'false')
INSERT INTO dbo.Book (BookName, BookPrice, BookGenre, BookDeleted) VALUES ('Pirati sa Kariba', 999, 3, 'false')
INSERT INTO dbo.Book (BookName, BookPrice, BookGenre, BookDeleted) VALUES ('Blago Cara Radovana', 750, 2, 'false')