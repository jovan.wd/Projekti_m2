﻿delete from Genre

DBCC CHECKIDENT ('Genre', RESEED, 1)
GO

INSERT INTO dbo.Genre (GenreName) VALUES ('Science');
INSERT INTO dbo.Genre (GenreName) VALUES ('Comedy');
INSERT INTO dbo.Genre (GenreName) VALUES ('Horror');