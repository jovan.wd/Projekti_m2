﻿create table Book
(
	BookId int identity primary key,
	BookName nvarchar(20),
	BookPrice decimal,
	BookGenre int,
	BookDeleted nvarchar(5),
	foreign key(BookGenre) references dbo.Genre(GenreId)
)