﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookStore.Controllers
{
    public class GenreController : Controller
    {
        
        public ActionResult Index()
        {
            string query = "SELECT * FROM Genre";  
            string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=BookStoreDB;Integrated Security=True";   

            DataTable dt = new DataTable(); 
            DataSet ds = new DataSet();

            List<Models.Genre> Genres = new List<Models.Genre>();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter(); 
                    dadapter.SelectCommand = cmd;                   

                    dadapter.Fill(ds, "Genre"); 
                    dt = ds.Tables["Genre"];   
                    con.Close();              
                }
            }
           

            foreach (DataRow dataRow in dt.Rows)            
            {
                int GenreId = int.Parse(dataRow["GenreId"].ToString());
                string GenreName = dataRow["GenreName"].ToString();

                Genres.Add(new Models.Genre() { Id = GenreId, Name = GenreName });
            }

            return View(Genres);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Models.Genre genre)
        {
            string query = "INSERT INTO Genre (GenreName) VALUES (@GenreName);";
            query += " SELECT SCOPE_IDENTITY()";       

            string connectionString = ConfigurationManager.ConnectionStrings["BookStoreDbContext"].ConnectionString;

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@GenreName", genre.Name);
                    con.Open();
                    var a = cmd.ExecuteScalar();              
                    con.Close();                                                        
                }
            }
            return RedirectToAction("Index"); 
        }
    }
}