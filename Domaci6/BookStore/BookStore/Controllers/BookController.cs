﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookStore.ViewModels;
using BookStore.Models;

namespace BookStore.Controllers
{
    public class BookController : Controller
    {
        public ActionResult Index()
        {
            string query = "SELECT BookId, BookName, BookPrice, GenreId, GenreName, BookDeleted FROM Book inner join Genre on BookGenre = GenreId";  // upit nad bazom
            string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=BookStoreDB;Integrated Security=True";    // da li je ovo dobra praksa?
            
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();    

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter(); 
                    dadapter.SelectCommand = cmd;                   

                   
                    dadapter.Fill(ds, "Book"); 
                    dt = ds.Tables["Book"];    
                    con.Close();                
                }
            }
            List<Models.Book> Books = new List<Models.Book>();

            foreach (DataRow dataRow in dt.Rows)         
            {
                int BookId = int.Parse(dataRow["BookId"].ToString());   
                string BookName = dataRow["BookName"].ToString();
                decimal BookPrice = decimal.Parse(dataRow["BookPrice"].ToString());
                Models.Genre genre = new Models.Genre();
                genre.Id = int.Parse(dataRow["GenreId"].ToString());
                genre.Name= dataRow["GenreName"].ToString();
                string BookDeleted = dataRow["BookDeleted"].ToString();
                Books.Add(new Models.Book() { Id = BookId, Name = BookName, Price=BookPrice, Genre= genre, Deleted=BookDeleted });
            }

            return View(Books);
        }

        [HttpGet]
        public ActionResult Create()
        {
            List<Models.Genre> Genres = new List<Models.Genre>();
            var vm = new BookGenreViewModel();
            
            string query = "SELECT * FROM Genre";
            string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=BookStoreDB;Integrated Security=True";

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;

                    dadapter.Fill(ds, "Genre");
                    dt = ds.Tables["Genre"];
                    con.Close();
                }
            }
            foreach (DataRow dataRow in dt.Rows)
            {
                int GenreId = int.Parse(dataRow["GenreId"].ToString());
                string GenreName = dataRow["GenreName"].ToString();

                Genres.Add(new Models.Genre() { Id = GenreId, Name = GenreName });
            }
            vm.Genres = Genres.ToList();
            return View(vm);
        }

        
        [HttpPost]
        public ActionResult Create(BookGenreViewModel vm)
        {
            string query = "INSERT INTO Book (BookName, BookPrice, BookGenre, BookDeleted) VALUES (@BookName, @BookPrice, @BookGenre, @BookDeleted);";
            query += "SELECT SCOPE_IDENTITY()";      

            string connectionString = ConfigurationManager.ConnectionStrings["BookStoreDbContext"].ConnectionString;

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@BookName", vm.Book.Name);
                    cmd.Parameters.AddWithValue("@BookPrice", vm.Book.Price);
                    cmd.Parameters.AddWithValue("@BookGenre", vm.SelectedGenreId);
                    cmd.Parameters.AddWithValue("@BookDeleted", vm.Book.Deleted);
                    con.Open();
                    cmd.ExecuteNonQuery();              
                    con.Close();                                                      
                }
            }
            
            return RedirectToAction("Index");  
        }

        [HttpGet]
        public ActionResult Deleted()
        {
            string query = "SELECT BookId, BookName, BookPrice, GenreId, GenreName, BookDeleted FROM Book inner join Genre on BookGenre = GenreId";  // upit nad bazom
            string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=BookStoreDB;Integrated Security=True";    // da li je ovo dobra praksa?

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;


                    dadapter.Fill(ds, "Book");
                    dt = ds.Tables["Book"];
                    con.Close();
                }
            }
            List<Models.Book> Books = new List<Models.Book>();

            foreach (DataRow dataRow in dt.Rows)
            {
                int BookId = int.Parse(dataRow["BookId"].ToString());
                string BookName = dataRow["BookName"].ToString();
                decimal BookPrice = decimal.Parse(dataRow["BookPrice"].ToString());
                Models.Genre genre = new Models.Genre();
                genre.Id = int.Parse(dataRow["GenreId"].ToString());
                genre.Name = dataRow["GenreName"].ToString();
                string BookDeleted = dataRow["BookDeleted"].ToString();
                Books.Add(new Models.Book() { Id = BookId, Name = BookName, Price = BookPrice, Genre = genre, Deleted = BookDeleted });
            }

            return View(Books);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            string query = "UPDATE Book SET BookDeleted = @BookDeleted where BookId=@BookId;";

            string connectionString = ConfigurationManager.ConnectionStrings["BookStoreDbContext"].ConnectionString;   // inicijaizuj novu konekciju

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    cmd.Parameters.AddWithValue("@BookId", id);
                    cmd.Parameters.AddWithValue("@BookDeleted", "true");  // stitimo od SQL Injection napada

                    con.Open();                                         // otvori konekciju
                    cmd.ExecuteNonQuery();              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();
                }
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            BookGenreViewModel vm = new BookGenreViewModel();
            List<Genre> Genres = new List<Models.Genre>();

            string query = "SELECT GenreId, GenreName from Genre";  
            string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=BookStoreDB;Integrated Security=True";   

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;  

                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;

                    dadapter.Fill(ds, "Genre");
                    dt = ds.Tables["Genre"];

                    con.Open();                                        
                    cmd.ExecuteNonQuery();              
                    con.Close();
                }
            }
            foreach (DataRow dataRow in dt.Rows)
            {
                Genre genre = new Genre();
                genre.Id = int.Parse(dataRow["GenreId"].ToString());
                genre.Name = dataRow["GenreName"].ToString();
                Genres.Add(genre);
            }
            vm.Genres = Genres;

            //***********************************************************************************************

            string query2 = "SELECT BookId, BookName, BookPrice,BookDeleted, GenreId, GenreName from Book inner join Genre on Book.BookGenre = Genre.GenreId where Book.BookId=@BookId";
           
            DataTable dt2 = new DataTable();
            DataSet ds2 = new DataSet();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query2;
                    cmd.Parameters.AddWithValue("@BookId", id);
                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;

                    dadapter.Fill(ds2, "Book");
                    dt2 = ds2.Tables["Book"];

                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            foreach (DataRow dataRow in dt2.Rows)
            {
                Book book = new Book();
                book.Id = id;
                book.Name = dataRow["BookName"].ToString();
                book.Price= decimal.Parse(dataRow["BookPrice"].ToString());
                book.Deleted= dataRow["BookDeleted"].ToString();
                Genre genre = new Genre();
                genre.Id = int.Parse(dataRow["GenreId"].ToString());
                genre.Name = dataRow["GenreName"].ToString();
                book.Genre = genre;
                vm.Book = book;
            }
            
            return View(vm);
        }

        [HttpPost]
        public ActionResult Edit(BookGenreViewModel vm)
        {
            string query = "UPDATE Book SET BookName=@BookName, BookPrice=@BookPrice, BookGenre=@BookGenre, BookDeleted=@BookDeleted where BookId=@BookId";

            string connectionString = ConfigurationManager.ConnectionStrings["BookStoreDbContext"].ConnectionString;   // inicijaizuj novu konekciju

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;   
                    cmd.Parameters.AddWithValue("@BookId", vm.Book.Id);
                    cmd.Parameters.AddWithValue("@BookName", vm.Book.Name); 
                    cmd.Parameters.AddWithValue("@BookPrice", vm.Book.Price);
                    cmd.Parameters.AddWithValue("@BookGenre", vm.SelectedGenreId);
                    cmd.Parameters.AddWithValue("@BookDeleted", vm.Book.Deleted);

                    con.Open();                                       
                    cmd.ExecuteScalar();             
                    con.Close(); 
                }
            }

            return RedirectToAction("Index");
        }

    }
}