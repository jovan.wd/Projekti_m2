﻿using eBanking.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eBanking.ViewModels
{
    public class UplatnicaRacunViewModel
    {
        public Racun Racun { get; set; }
        public Uplatnica Uplatnica { get; set; }
        public IEnumerable<Racun> Racuni { get; set; }
        public IEnumerable<Uplatnica> Uplatnice { get; set; }
        public int SelectedRacunId { get; set; }
        public IEnumerable<Racun> RacuniAktivni { get; set; }
    }
}