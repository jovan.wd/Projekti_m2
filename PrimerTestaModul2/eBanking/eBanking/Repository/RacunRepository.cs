﻿using eBanking.Models;
using eBanking.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace eBanking.Repository
{
    public class RacunRepository : IRacunRepository
    {
        private SqlConnection connection;

        private void CreateConnection()
        {
            string constr = ConfigurationManager.ConnectionStrings["eBankingDBContext"].ToString();
            connection = new SqlConnection(constr);
        }

        public bool Create(Racun racun)
        {
            string query = "INSERT INTO Racun (RacunNosilac, RacunBroj, RacunAktivan, RacunOnlineBanking) VALUES (@RacunNosilac, @RacunBroj, @RacunAktivan, @RacunOnlineBanking);";
            query += " SELECT SCOPE_IDENTITY()";

            CreateConnection();

            using (SqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query;
                cmd.Parameters.AddWithValue("@RacunNosilac", racun.Nosilac);
                cmd.Parameters.AddWithValue("@RacunBroj", racun.Broj);
                cmd.Parameters.AddWithValue("@RacunAktivan", racun.Aktivan);
                cmd.Parameters.AddWithValue("@RacunOnlineBanking", racun.OnlineBanking);

                connection.Open();
                var newFormedId = cmd.ExecuteScalar();
                connection.Close();

                if (newFormedId != null)
                {
                    return true;
                }
            }
            return false;
        }

        public void Delete(int id)
        {
            string query = "DELETE FROM Racun WHERE RacunId = @Id;";

            CreateConnection();

            using (SqlCommand sqlCommand = connection.CreateCommand())
            {
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("Id", id);

                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
        }

        public IEnumerable<Racun> GetAll()
        {
            string query = "SELECT * FROM Racun ";
                
            CreateConnection();

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            using (SqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query;

                SqlDataAdapter dadapter = new SqlDataAdapter();
                dadapter.SelectCommand = cmd;


                dadapter.Fill(ds, "Racun");
                dt = ds.Tables["Racun"];
                connection.Close();
            }

            List<Racun> sviRacuni = new List<Racun>();

            foreach (DataRow dataRow in dt.Rows)
            {
                int racunId = int.Parse(dataRow["RacunId"].ToString());
                string racunNosilac = dataRow["RacunNosilac"].ToString();
                int racunBroj = int.Parse(dataRow["RacunBroj"].ToString());
                bool racunAktivan = bool.Parse(dataRow["RacunAktivan"].ToString());
                bool racunOnlineBanking = bool.Parse(dataRow["RacunOnlineBanking"].ToString());
               
                sviRacuni.Add(new Racun() { Id = racunId, Nosilac = racunNosilac, Broj = racunBroj, Aktivan= racunAktivan, OnlineBanking = racunOnlineBanking});
            }

            return sviRacuni;
        }

        public Racun GetById(int id)
        {
            string query = "SELECT * FROM Racun  WHERE RacunId = @Id;";
            CreateConnection();

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            using (SqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query;
                cmd.Parameters.AddWithValue("@Id", id);

                SqlDataAdapter dadapter = new SqlDataAdapter();
                dadapter.SelectCommand = cmd;


                dadapter.Fill(ds, "Racun");
                dt = ds.Tables["Racun"];
                connection.Close();
            }

            Racun racun = null;

            foreach (DataRow dataRow in dt.Rows)
            {
                int racunId = int.Parse(dataRow["RacunId"].ToString());
                string racunNosilac = dataRow["RacunNosilac"].ToString();
                int racunBroj = int.Parse(dataRow["RacunBroj"].ToString());
                bool racunAktivan = bool.Parse(dataRow["RacunAktivan"].ToString());
                bool racunOnlineBanking = bool.Parse(dataRow["RacunOnlineBanking"].ToString());

                racun = new Racun() { Id = racunId, Nosilac = racunNosilac, Broj = racunBroj, Aktivan = racunAktivan, OnlineBanking = racunOnlineBanking };
            }

            return racun;

        }

        public void Update(Racun racun)
        {
            string query = "UPDATE Racun set RacunNosilac = @nosilac, RacunBroj = @broj, @RacunAktivan = @aktivan, RacunOnlineBanking = @onlineBanking  where RacuinId = @Id;";

            CreateConnection();

            using (SqlCommand sqlCommand = connection.CreateCommand())
            {
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@Id", racun.Id);
                sqlCommand.Parameters.AddWithValue("@nosilac", racun.Nosilac);
                sqlCommand.Parameters.AddWithValue("@broj", racun.Broj);
                sqlCommand.Parameters.AddWithValue("@aktivan", racun.Aktivan);
                sqlCommand.Parameters.AddWithValue("@onlineBanking", racun.OnlineBanking);

                connection.Open();
                sqlCommand.ExecuteScalar();
                connection.Close();
            }
        }

        public void Aktiviraj(int id)
        {
            string query = "UPDATE Racun set RacunAktivan = @aktivan where RacunId = @Id;";

            CreateConnection();

            using (SqlCommand sqlCommand = connection.CreateCommand())
            {
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@Id", id);
                sqlCommand.Parameters.AddWithValue("@aktivan", true);

                connection.Open();
                sqlCommand.ExecuteScalar();
                connection.Close();
            }
        }

        public void Deaktiviraj(int id)
        {
            string query = "UPDATE Racun set RacunAktivan = @aktivan where RacunId = @Id;";

            CreateConnection();

            using (SqlCommand sqlCommand = connection.CreateCommand())
            {
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@Id", id);
                sqlCommand.Parameters.AddWithValue("@aktivan", false);

                connection.Open();
                sqlCommand.ExecuteScalar();
                connection.Close();
            }
        }
    }
}