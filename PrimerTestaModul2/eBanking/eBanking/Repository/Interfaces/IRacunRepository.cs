﻿using eBanking.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eBanking.Repository.Interfaces
{
    interface IRacunRepository
    {
        IEnumerable<Racun> GetAll();
        Racun GetById(int id);
        bool Create(Racun racun);
        void Update(Racun racun);
        void Delete(int id);
        void Aktiviraj(int id);
        void Deaktiviraj(int id);
    }
}
