﻿using eBanking.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eBanking.Repository.Interfaces
{
    interface IUplatnicaRepository
    {
        IEnumerable<Uplatnica> GetAll();
        Uplatnica GetById(int id);
        bool Create(Uplatnica uplatnica);
        void Update(Uplatnica uplatnica);
        void Delete(int id);
    }
}
