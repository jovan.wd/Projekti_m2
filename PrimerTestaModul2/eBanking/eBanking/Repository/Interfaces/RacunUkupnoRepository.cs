﻿using eBanking.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace eBanking.Repository.Interfaces
{
    public class RacunUkupnoRepository : IRacunUkupnoRepository
    {
        private SqlConnection connection;

        private void CreateConnection()
        {
            string constr = ConfigurationManager.ConnectionStrings["eBankingDBContext"].ToString();
            connection = new SqlConnection(constr);
        }

        public double GetUkupnoById(int id)
        {
            double ukupno=0;
            string query = "SELECT r.RacunId, sum(u.UplatnicaIznosUplate) as Sum from Racun r join Uplatnica u " +
                "on u.UplatnicaRacunId = r.RacunId where r.RacunId = @id" +
                "group by r.RacunId";

            CreateConnection();

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            using (SqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query;
                cmd.Parameters.AddWithValue("@id", id);

                SqlDataAdapter dadapter = new SqlDataAdapter();
                dadapter.SelectCommand = cmd;
                
                dadapter.Fill(ds, "Racun");
                dt = ds.Tables["Racun"];
                connection.Close();
            }
            foreach (DataRow dataRow in dt.Rows)
            {
                int racunId = int.Parse(dataRow["RacunId"].ToString());
                ukupno += double.Parse(dataRow["Sum"].ToString());
                
            }
            return ukupno;
        }
    }
}