﻿using eBanking.Models;
using eBanking.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace eBanking.Repository
{
    public class UplatnicaRepository: IUplatnicaRepository
    {
        private SqlConnection connection;
        private IRacunRepository racunRepo = new RacunRepository();

        private void CreateConnection()
        {
            string constr = ConfigurationManager.ConnectionStrings["eBankingDBContext"].ToString();
            connection = new SqlConnection(constr);
        }

        public bool Create(Uplatnica uplatnica)
        {
            string query = "INSERT INTO Uplatnica (UplatnicaIznosUplate, UplatnicaDatumPrometa, UplatnicaSvrhaUplate, UplatnicaUplatilac, UplatnicaHitno, UplatnicaRacunId) " +
                           "VALUES (@UplatnicaIznosUplate, @UplatnicaDatumPrometa, @UplatnicaSvrhaUplate, @UplatnicaUplatilac, @UplatnicaHitno, @UplatnicaRacunId);";
            query += " SELECT SCOPE_IDENTITY()";

            CreateConnection();

            using (SqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query;
                cmd.Parameters.AddWithValue("@UplatnicaIznosUplate", uplatnica.IznosUplate);
                cmd.Parameters.AddWithValue("@UplatnicaDatumPrometa", uplatnica.DatumUplate);
                cmd.Parameters.AddWithValue("@UplatnicaSvrhaUplate", uplatnica.SvrhaUplate);
                cmd.Parameters.AddWithValue("@UplatnicaUplatilac", uplatnica.Uplatilac);
                cmd.Parameters.AddWithValue("@UplatnicaHitno", uplatnica.Hitno);
                cmd.Parameters.AddWithValue("@UplatnicaRacunId", uplatnica.Racun.Id);

                connection.Open();
                var newFormedId = cmd.ExecuteScalar();
                connection.Close();

                if (newFormedId != null)
                {
                    return true;
                }
            }
            return false;
        }

        public void Delete(int id)
        {
            string query = "DELETE FROM Uplatnica WHERE UplatnicaId = @Id;";

            CreateConnection();

            using (SqlCommand sqlCommand = connection.CreateCommand())
            {
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("Id", id);

                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
        }

        public IEnumerable<Uplatnica> GetAll()
        {
            string query = "SELECT * FROM Uplatnica u, Racun r where u.UplatnicaRacunId = r.RacunId; ";
                
            CreateConnection();

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            using (SqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query;

                SqlDataAdapter dadapter = new SqlDataAdapter();
                dadapter.SelectCommand = cmd;


                dadapter.Fill(ds, "Uplatnica");
                dt = ds.Tables["Uplatnica"];
                connection.Close();
            }

            List<Uplatnica> uplatnice = new List<Uplatnica>();

            foreach (DataRow dataRow in dt.Rows)            
            {
                //podaci Uplatnice
                int uplatnicaId = int.Parse(dataRow["UplatnicaId"].ToString());  
                double uplatnicaIznosUplate = double.Parse(dataRow["UplatnicaIznosUplate"].ToString());
                string uplatnicaSvrhaUplate = dataRow["UplatnicaSvrhaUplate"].ToString();
                DateTime uplatnicaDatumPrometa = DateTime.Parse(dataRow["UplatnicaDatumPrometa"].ToString());
                string uplatnicaUplatilac = dataRow["UplatnicaUplatilac"].ToString();
                bool uplatnicaHitno = bool.Parse(dataRow["UplatnicaHitno"].ToString());

                //podacu Racuna
                int racunId = int.Parse(dataRow["RacunId"].ToString());
                string racunNosilac = dataRow["RacunNosilac"].ToString();
                int racunBroj = int.Parse(dataRow["RacunBroj"].ToString());
                bool racunAktivan = bool.Parse(dataRow["RacunAktivan"].ToString());
                bool racunOnlineBanking = bool.Parse(dataRow["RacunOnlineBanking"].ToString());

                Racun racun = new Racun
                {
                    Id = racunId,
                    Nosilac = racunNosilac,
                    Broj = racunBroj,
                    Aktivan = racunAktivan,
                    OnlineBanking = racunOnlineBanking
                };

                uplatnice.Add(new Uplatnica() { Id = uplatnicaId, IznosUplate = uplatnicaIznosUplate, SvrhaUplate = uplatnicaSvrhaUplate, Uplatilac = uplatnicaUplatilac, DatumUplate = uplatnicaDatumPrometa, Hitno = uplatnicaHitno, Racun = racun });
            }

            return uplatnice;
        }

        public Uplatnica GetById(int id)
        {
            string query = "SELECT * FROM Uplatnica u, Racun r WHERE u.UplatnicaId = @UplatnicaId and r.RacunId = u.UplatnicaRacunId;";
            CreateConnection();

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            using (SqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query;
                cmd.Parameters.AddWithValue("@UplatnicaId", id);

                SqlDataAdapter dadapter = new SqlDataAdapter();
                dadapter.SelectCommand = cmd;

                dadapter.Fill(ds, "Uplatnica");
                dt = ds.Tables["Uplatnica"];
                connection.Close();
            }

            Uplatnica uplatnica = null;

            foreach (DataRow dataRow in dt.Rows)
            {
                //podaci Uplatnice
                int uplatnicaId = int.Parse(dataRow["UplatnicaId"].ToString());
                double uplatnicaIznosUplate = double.Parse(dataRow["UplatnicaIznosUplate"].ToString());
                string uplatnicaSvrhaUplate = dataRow["UplatnicaSvrhaUplate"].ToString();
                DateTime uplatnicaDatumPrometa = DateTime.Parse(dataRow["UplatnicaDatumPrometa"].ToString());
                string uplatnicaUplatilac = dataRow["UplatnicaUplatilac"].ToString();
                bool uplatnicaHitno = bool.Parse(dataRow["UplatnicaHitno"].ToString());

                //podacu Racuna
                int racunId = int.Parse(dataRow["RacunId"].ToString());
                string racunNosilac = dataRow["RacunNosilac"].ToString();
                int racunBroj = int.Parse(dataRow["RacunBroj"].ToString());
                bool racunAktivan = bool.Parse(dataRow["RacunAktivan"].ToString());
                bool racunOnlineBanking = bool.Parse(dataRow["RacunOnlineBanking"].ToString());

                Racun racun = new Racun
                {
                    Id = racunId,
                    Nosilac = racunNosilac,
                    Broj = racunBroj,
                    Aktivan = racunAktivan,
                    OnlineBanking = racunOnlineBanking
                };

                uplatnica = new Uplatnica { Id = uplatnicaId, IznosUplate = uplatnicaIznosUplate, SvrhaUplate = uplatnicaSvrhaUplate, Uplatilac = uplatnicaUplatilac, DatumUplate = uplatnicaDatumPrometa, Hitno = uplatnicaHitno, Racun = racun };
            }

            return uplatnica;

        }

        public void Update(Uplatnica uplatnica)
        {
            string query = "UPDATE Uplatnica set UplatnicaIznosUplate = @iznosUplate, UplatnicaDatumPrometa = @datumPrometa, UplatnicaSvrhaUplate = @svrhaUplate, UplatnicaUplatilac = @uplatilac, UplatnicaHitno = @hitno, UplatnicaRacunId = @racunId where UplatnicaId = @id";

            CreateConnection();

            using (SqlCommand sqlCommand = connection.CreateCommand())
            {
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@id", uplatnica.Id);
                sqlCommand.Parameters.AddWithValue("@iznosUplate", uplatnica.IznosUplate);
                sqlCommand.Parameters.AddWithValue("@datumPrometa", uplatnica.DatumUplate);
                sqlCommand.Parameters.AddWithValue("@svrhaUplate", uplatnica.SvrhaUplate);
                sqlCommand.Parameters.AddWithValue("@uplatilac", uplatnica.Uplatilac);
                sqlCommand.Parameters.AddWithValue("@hitno", uplatnica.Hitno);
                sqlCommand.Parameters.AddWithValue("@racunId", uplatnica.Racun.Id);

                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
        }
    }
}