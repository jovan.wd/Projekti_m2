﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eBanking.Models
{
    public class Racun
    {
        public int Id { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 3)]
        [RegularExpression("[A-Z][A-Za-z ]*")]
        public string Nosilac { get; set; }

        [Range(111111111, 999999999)]
        public int Broj { get; set; }

        public bool Aktivan { get; set; }
        public bool OnlineBanking { get; set; }
        public double Ukupno { get; set; }
    }
}