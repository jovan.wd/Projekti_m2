﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eBanking.Models
{
    public class Uplatnica
    {
        public int Id { get; set; }
        public Racun Racun { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 3)]
        [RegularExpression("[A-Z][A-Za-z ]*")]
        public string Uplatilac { get; set; }

        [Range(1, Int32.MaxValue)]
        public double IznosUplate { get; set; }
        public DateTime DatumUplate { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 3)]
        [RegularExpression("[A-Z][A-Za-z ]*")]
        public string SvrhaUplate { get; set; }
        public bool Hitno { get; set; }
    }
}