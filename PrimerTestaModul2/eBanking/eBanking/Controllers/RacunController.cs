﻿using eBanking.Models;
using eBanking.Repository;
using eBanking.Repository.Interfaces;
using eBanking.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eBanking.Controllers
{
    public class RacunController : Controller
    {
        private IRacunRepository racunRepo = new RacunRepository();
        private IUplatnicaRepository uplatnicaRepo = new UplatnicaRepository();
        // GET: Racun
        public ActionResult Index()
        {
            UplatnicaRacunViewModel vm = new UplatnicaRacunViewModel();
            var racuni = racunRepo.GetAll();
            var uplatnice = uplatnicaRepo.GetAll();

            foreach (Racun r in racuni)
            {
                double ukupno = 0;
                foreach (Uplatnica u in uplatnice)
                {
                    if (u.Racun.Id == r.Id)
                    {
                        ukupno += u.IznosUplate;
                    }
                }
                r.Ukupno = ukupno;
            }
            vm.Racuni = racuni;
            return View(vm);
        }
        
        // GET: Racun/Create
        public ActionResult Create()
        {
            return View(new Racun());
        }

        // POST: Racun/Create
        [HttpPost]
        public ActionResult Create(Racun racun)
        {
            if (ModelState.IsValid)
            {
                racunRepo.Create(racun);
                return Redirect("Index");
            }

            return View();
        }

        // GET: Racun/Edit/5
        public ActionResult Edit(int id)
        {
            var racun = racunRepo.GetById(id);

            return View(racun);
        }

        // POST: Racun/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            var racun = new Racun();

            try
            {
                UpdateModel(racun);

                racunRepo.Update(racun);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(racun);
            }
        }

        // GET: TipMotora/Delete/5
        public ActionResult Delete(int id)
        {
            List<Uplatnica> uplatnice = new List<Uplatnica>();
            uplatnice= uplatnicaRepo.GetAll().ToList();
         
            try
            {
                foreach (Uplatnica u in uplatnice)
                {
                    if (u.Racun.Id == id)
                    {
                        uplatnicaRepo.Delete(u.Id);
                    }
                }
                racunRepo.Delete(id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }

            return RedirectToAction("Index");
        }

        public ActionResult Aktiviraj(int id)
        {
            try
            {
                racunRepo.Aktiviraj(id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }

            return RedirectToAction("Index");
        }

        public ActionResult Deaktiviraj(int id)
        {
            try
            {
                racunRepo.Deaktiviraj(id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Search()
        {
            var vm = new UplatnicaRacunViewModel();
            var racuni = racunRepo.GetAll();
            var uplatnice = uplatnicaRepo.GetAll();
            vm.Racuni = racuni;
            vm.Uplatnice = uplatnice;
            return View(vm);
        }

        [HttpPost]
        public ActionResult Search(string Kriterijum, string Naziv)
        {
            var racuni = racunRepo.GetAll();
            var uplatnice = uplatnicaRepo.GetAll();
            var aktivni = new List<Racun>();
            foreach (Racun r in racuni)
            {
                double ukupno = 0;
                foreach (Uplatnica u in uplatnice)
                {
                    if (u.Racun.Id == r.Id)
                    {
                        ukupno += u.IznosUplate;
                    }
                }
                r.Ukupno = ukupno;
            }
            Racun racun = null;
            if (Naziv == "nosilac racuna")
            {
                foreach (var b in racuni)
                {
                    if (b.Nosilac.Contains(Kriterijum))
                    {
                        racun = b;
                    }
                }
            }
            else if (Naziv == "broj racuna")
            {
                foreach (var b in racuni)
                {
                    if (b.Broj == int.Parse(Kriterijum))
                    {
                        racun = b;
                    }
                }
            }
            else if (Naziv == "aktivan")
            {
                bool aktivan=true;
                if (Kriterijum == "Ne") { aktivan = false; }
                foreach (var b in racuni)
                {
                    if (b.Aktivan == aktivan)
                    {
                        aktivni.Add(b);
                    }
                }
            }
            
            var viewModel = new UplatnicaRacunViewModel();
            viewModel.Racun = racun;
            viewModel.RacuniAktivni = aktivni;

            return View(viewModel);
        }
    }
}