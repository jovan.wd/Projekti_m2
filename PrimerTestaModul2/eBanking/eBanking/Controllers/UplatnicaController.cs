﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eBanking.Models;
using eBanking.Repository;
using eBanking.Repository.Interfaces;
using eBanking.ViewModels;

namespace eBanking.Controllers
{
    public class UplatnicaController : Controller
    {
        private IRacunRepository racunRepo = new RacunRepository();
        private IUplatnicaRepository uplatnicaRepo = new UplatnicaRepository();
        // GET: Uplatnica
        public ActionResult Index(int id)
        {
            UplatnicaRacunViewModel vm = new UplatnicaRacunViewModel();
            var racuni = racunRepo.GetAll();
            var uplatnice = uplatnicaRepo.GetAll();
            List<Uplatnica> upl = new List<Uplatnica>();
            double ukupno = 0;
            foreach (Uplatnica u in uplatnice)
            {
                if (u.Racun.Id == id)
                {
                    ukupno += u.IznosUplate;
                    upl.Add(u);
                }
            }

            vm.Uplatnice = upl;
           
            foreach (Racun r in racuni)
            {
                if (r.Id == id)
                {
                    vm.Racun = r;
                }
            }
            vm.Racun.Ukupno = ukupno;
            return View(vm);
        }

        public ActionResult Details(int id)
        {
            var uplatnica = uplatnicaRepo.GetById(id);
            return View(uplatnica);
        }
        
        public ActionResult Create()
        {
            var racuni = racunRepo.GetAll();

            UplatnicaRacunViewModel vm = new UplatnicaRacunViewModel()
            {
                Racuni = racuni.ToList()
            };

            return View(vm);
        }

        [HttpPost]
        public ActionResult Create(Uplatnica uplatnica)
        {
            uplatnicaRepo.Create(uplatnica);
            return RedirectToAction("Index", "Racun");
        }
        
        public ActionResult Edit(int id)
        {
            var uplatnica = uplatnicaRepo.GetById(id);
            var racuni = racunRepo.GetAll();

            var vm = new UplatnicaRacunViewModel
            {
                Uplatnica = uplatnica,
                Racuni = racuni.ToList()
            };

            return View(vm);
        }
        
        [HttpPost]
        public ActionResult Edit(int id, UplatnicaRacunViewModel vm)
        {
            
                string uplatilac = vm.Uplatnica.Uplatilac;
                double iznos = vm.Uplatnica.IznosUplate;
                DateTime datum = vm.Uplatnica.DatumUplate;
                string svrha = vm.Uplatnica.SvrhaUplate;
                bool hitno = vm.Uplatnica.Hitno;
                int racunId = vm.Uplatnica.Racun.Id;

                var updateUplatnica = new Uplatnica() { Id = id, Uplatilac = uplatilac, DatumUplate = datum, IznosUplate = iznos, SvrhaUplate = svrha, Hitno = hitno, Racun = racunRepo.GetById(racunId) };

                uplatnicaRepo.Update(updateUplatnica);

                return RedirectToAction("Index", "Racun");
            

        }
        
        public ActionResult Delete(int id)
        {
            uplatnicaRepo.Delete(id);
            return RedirectToAction("Index", "Racun");
        }
    }
}