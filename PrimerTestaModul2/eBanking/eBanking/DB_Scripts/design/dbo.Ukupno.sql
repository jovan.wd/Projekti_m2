﻿create table Ukupno 
(
	Ukupnoid int identity primary key ,
	UkupnoRacunId int,
	UkupnoUkupno numeric,
	foreign key (UkupnoRacunId) references Racun(RacunId)
)