﻿
CREATE TABLE Racun
(
	RacunId INT IDENTITY(1,1) PRIMARY KEY,
	RacunNosilac NVARCHAR(80) NULL, 
    RacunBroj int,
	RacunAktivan bit,
	RacunOnlineBanking bit,
)