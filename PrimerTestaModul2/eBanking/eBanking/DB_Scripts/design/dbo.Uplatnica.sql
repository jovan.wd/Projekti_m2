﻿
CREATE TABLE Uplatnica
(
	UplatnicaId INT IDENTITY(1,1) PRIMARY KEY,
	UplatnicaRacunId int,
    UplatnicaIznosUplate numeric(15,2),
	UplatnicaDatumPrometa datetime,
	UplatnicaSvrhaUplate nvarchar(200),
	UplatnicaUplatilac nvarchar(200),
	UplatnicaHitno bit,
	FOREIGN KEY (UplatnicaRacunId) REFERENCES Racun(RacunId)
)