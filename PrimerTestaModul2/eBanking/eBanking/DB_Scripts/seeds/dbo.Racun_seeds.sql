﻿delete from Racun
DBCC CHECKIDENT ('[Racun]', RESEED, 0);
GO

insert into Racun(RacunNosilac, RacunBroj, RacunAktivan, RacunOnlineBanking)
values
	('Petar Petrovic', 111222333, 1, 1),
	('Jovan Jovanovic', 222111333, 1, 0),
	('Mitar Mitra Mitar', 333333111, 0, 1),
	('Boza Derikoza', 999666333, 0, 1)
