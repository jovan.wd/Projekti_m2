﻿delete from Uplatnica
DBCC CHECKIDENT ('[Uplatnica]', RESEED, 0);
GO

insert into Uplatnica(UplatnicaRacunId, UplatnicaIznosUplate, UplatnicaDatumPrometa, UplatnicaSvrhaUplate, UplatnicaUplatilac, UplatnicaHitno)
values
	(1, 3256.50, '2012-06-18 06:30:56', 'Deciji dodatak', 'Nikola Nikolic', 1 ),
	(1, 999.66, '2015-02-22 11:39:59', 'Naknada', 'Sanja Sanjic', 1 ),
	(2, -10000, '2011-11-17 17:59:56', 'Struja', 'Elektro-Vojvodina', 1 ),
	(2, 12000.50, '2006-06-16 10:36:33', 'Regres', 'Pavle Pavlicic', 1 ),
	(3, 5555.55, '2014-08-19 08:30:02', 'Plata', 'Nikola Nikolic', 1 ),
	(4, 3333.33, '2011-03-18 10:39:09', 'Plata', 'Nikola Nikolic', 1 ),
	(4, -1500, '2016-02-29 12:01:01', 'Rata za parike', 'Sport Vision', 1 )
