﻿using AutoMotori.Models;
using AutoMotori.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AutoMotori.Repository
{
    public class TipMotoraRepository : ITipMotoraRepository
    {
        private SqlConnection connection;

        private void CreateConnection()
        {
            string constr = ConfigurationManager.ConnectionStrings["AutomobiliContext"].ToString();
            connection = new SqlConnection(constr);
        }

        public bool Create(TipMotora tipMotora)
        {
            string query = "INSERT INTO TipMotora (TipMotoraNaziv) VALUES (@TipMotoraNaziv);";
            query += " SELECT SCOPE_IDENTITY()";        

            CreateConnection();   

            using (SqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query;    
                cmd.Parameters.AddWithValue("@TipMotoraNaziv", tipMotora.Naziv);  

                connection.Open();                                       
                var newFormedId = cmd.ExecuteScalar();              
                connection.Close();                                       

                if (newFormedId != null)
                {
                    return true;  
                }
            }
            return false;  
        }

        public void Delete(int id)
        {
            string query = "DELETE FROM TipMotora WHERE TipMotoraId = @Id;";

            CreateConnection();

            using (SqlCommand sqlCommand = connection.CreateCommand())
            {
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("Id", id);

                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
        }

        public IEnumerable<TipMotora> GetAll()
        {
            string query = "SELECT * FROM TipMotora;";
            CreateConnection();   

            DataTable dt = new DataTable(); 
            DataSet ds = new DataSet();    

            using (SqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query;

                SqlDataAdapter dadapter = new SqlDataAdapter(); 
                dadapter.SelectCommand = cmd;                  

                
                dadapter.Fill(ds, "TipMotora"); 
                dt = ds.Tables["TipMotora"];    
                connection.Close();                  
            }

            List<TipMotora> tipoviMotora = new List<TipMotora>();

            foreach (DataRow dataRow in dt.Rows)           
            {
                int tipMotoraId = int.Parse(dataRow["TipMotoraId"].ToString());    
                string tipMotoraNaziv = dataRow["TipMotoraNaziv"].ToString();

                tipoviMotora.Add(new TipMotora() { Id = tipMotoraId, Naziv = tipMotoraNaziv });
            }

            return tipoviMotora;
        }

        public TipMotora GetById(int id)
        {
            string query = "SELECT * FROM TipMotora  WHERE TipMotoraId = @TipMotoraId;";
            CreateConnection();  

            DataTable dt = new DataTable(); 
            DataSet ds = new DataSet();     

            using (SqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query;
                cmd.Parameters.AddWithValue("@TipMotoraId", id);

                SqlDataAdapter dadapter = new SqlDataAdapter(); 
                dadapter.SelectCommand = cmd;                   

                
                dadapter.Fill(ds, "TipMotora"); 
                dt = ds.Tables["TipMotora"];   
                connection.Close();                 
            }

            TipMotora tipMotora = null;

            foreach (DataRow dataRow in dt.Rows)            
            {
                int tipMotoraId = int.Parse(dataRow["TipMotoraId"].ToString());
                string tipMotoraNaziv = dataRow["TipMotoraNaziv"].ToString();

                tipMotora = new TipMotora() { Id = tipMotoraId, Naziv = tipMotoraNaziv };
            }

            return tipMotora;

        }

        public void Update(TipMotora tipMotora)
        {
            string query = "UPDATE TipMotora set TipMotoraNaziv = @TipMotoraNaziv where TipMotoraId = @TipMotoraId;";

            CreateConnection();

            using (SqlCommand sqlCommand = connection.CreateCommand())
            {
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@TipMotoraNaziv", tipMotora.Naziv);
                sqlCommand.Parameters.AddWithValue("@TipMotoraId", tipMotora.Id);

                connection.Open();
                sqlCommand.ExecuteScalar();
                connection.Close();
            }
        }
    }
}