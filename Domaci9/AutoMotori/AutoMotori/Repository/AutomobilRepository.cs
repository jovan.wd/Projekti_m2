﻿using AutoMotori.Models;
using AutoMotori.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AutoMotori.Repository
{
    public class AutomobilRepository : IAutomobilRepository
    {
        private SqlConnection connection;
        private ITipMotoraRepository tipMotoraRepo = new TipMotoraRepository();

        private void CreateConnection()
        {
            string constr = ConfigurationManager.ConnectionStrings["AutomobiliContext"].ToString();
            connection = new SqlConnection(constr);
        }

        public bool Create(Automobil automobil)
        {
            string query = "INSERT INTO Automobil (AutomobilProizvodjac, AutomobilModel, AutomobilGodisete, AutomobilZapremina, AutomobilBoja, AutomobilTipMotoraId) " +
                           "VALUES (@AutomobilProizvodjac, @AutomobilModel, @AutomobilGodiste, @AutomobilZapremina, @AutomobilBoja, @AutomobilTipMotoraId);";
            query += " SELECT SCOPE_IDENTITY()";       

            CreateConnection();   

            using (SqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query;   
                cmd.Parameters.AddWithValue("@AutomobilProizvodjac", automobil.Proizvodjac);
                cmd.Parameters.AddWithValue("@AutomobilModel", automobil.Model);
                cmd.Parameters.AddWithValue("@AutomobilGodiste", automobil.Godiste);
                cmd.Parameters.AddWithValue("@AutomobilZapremina", automobil.Zapremina);
                cmd.Parameters.AddWithValue("@AutomobilBoja", automobil.Boja);
                cmd.Parameters.AddWithValue("@AutomobilTipMotoraId", automobil.TipMotora.Id);

                connection.Open();                                       
                var newFormedId = cmd.ExecuteScalar();              
                connection.Close();                                       

                if (newFormedId != null)
                {
                    return true;   
                }
            }
            return false;   
        }

        public void Delete(int id)
        {
            string query = "DELETE FROM Automobil WHERE AutomobilId = @Id;";

            CreateConnection();

            using (SqlCommand sqlCommand = connection.CreateCommand())
            {
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("Id", id);

                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
        }

        public IEnumerable<Automobil> GetAll()
        {
            string query = "SELECT * FROM Automobil a, TipMotora tm where a.AutomobilTipMotoraId = tm.TipMotoraId;";
            CreateConnection();  

            DataTable dt = new DataTable(); 
            DataSet ds = new DataSet();     

            using (SqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query;

                SqlDataAdapter dadapter = new SqlDataAdapter();
                dadapter.SelectCommand = cmd;                   

                
                dadapter.Fill(ds, "Automobil"); 
                dt = ds.Tables["Automobil"];    
                connection.Close();                  
            }

            List<Automobil> automobili = new List<Automobil>();

            foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
            {
                int autoId = int.Parse(dataRow["AutomobilId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                string autoProizvodjac = dataRow["AutomobilProizvodjac"].ToString();
                string autoModel = dataRow["AutomobilModel"].ToString();
                int autoGodiste = int.Parse(dataRow["AutomobilGodisete"].ToString());
                int autoZapremina = int.Parse(dataRow["AutomobilZapremina"].ToString());
                string autoBoja = dataRow["AutomobilBoja"].ToString();
                int tipMotoraId= int.Parse(dataRow["TipMotoraId"].ToString());
                string tipMotoraNaziv = dataRow["TipMotoraNaziv"].ToString();

                TipMotora tm = new TipMotora
                {
                    Id = tipMotoraId,
                    Naziv = tipMotoraNaziv
                };

                automobili.Add(new Automobil() { Id = autoId, Proizvodjac = autoProizvodjac, Model = autoModel, Godiste = autoGodiste, Zapremina=autoZapremina, Boja=autoBoja, TipMotora= tm });
            }

            return automobili;
        }

        public Automobil GetById(int id)
        {
            string query = "SELECT * FROM Automobil a, TipMotora tm WHERE a.AutomobilId = @AutomobilId and tm.TipMotoraId = a.AutomobilTipMotoraId;";
            CreateConnection();   

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();  

            using (SqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query;
                cmd.Parameters.AddWithValue("AutomobilId", id);

                SqlDataAdapter dadapter = new SqlDataAdapter(); 
                dadapter.SelectCommand = cmd;                  

                dadapter.Fill(ds, "Automobil"); 
                dt = ds.Tables["Automobil"];    
                connection.Close();                  
            }

            Automobil automobil = null;

            foreach (DataRow dataRow in dt.Rows)           
            {
                int autoId = int.Parse(dataRow["AutomobilId"].ToString());    
                string autoProizvodjac = dataRow["AutomobilProizvodjac"].ToString();
                string autoModel = dataRow["AutomobilModel"].ToString();
                int autoGodiste = int.Parse(dataRow["AutomobilGodisete"].ToString());
                int autoZapremina = int.Parse(dataRow["AutomobilZapremina"].ToString());
                string autoBoja = dataRow["AutomobilBoja"].ToString();
                int tipMotoraId = int.Parse(dataRow["TipMotoraId"].ToString());
                string tipMotoraNaziv = dataRow["TipMotoraNaziv"].ToString();

                TipMotora tm = new TipMotora
                {
                    Id = tipMotoraId,
                    Naziv = tipMotoraNaziv
                };

                automobil= new Automobil { Id = autoId, Proizvodjac = autoProizvodjac, Model = autoModel, Godiste = autoGodiste, Zapremina = autoZapremina, Boja = autoBoja, TipMotora = tm };
            }

            return automobil;

        }

        public void Update(Automobil automobil)
        {
            string query = "UPDATE Automobil set AutomobilProizvodjac = @AutomobilProizvodjac, AutomobilModel = @AutomobilModel, AutomobilGodisete = @AutomobilGodiste, AutomobilZapremina = @AutomobilZapremina, AutomobilBoja = @AutomobilBoja, AutomobilTipMotoraId = @AutomobilTipMotoraId where AutomobilId = @id";

            CreateConnection();

            using (SqlCommand sqlCommand = connection.CreateCommand())
            {
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@id", automobil.Id);
                sqlCommand.Parameters.AddWithValue("@AutomobilProizvodjac", automobil.Proizvodjac);
                sqlCommand.Parameters.AddWithValue("@AutomobilModel", automobil.Model);
                sqlCommand.Parameters.AddWithValue("@AutomobilGodiste", automobil.Godiste);
                sqlCommand.Parameters.AddWithValue("@AutomobilZapremina", automobil.Zapremina);
                sqlCommand.Parameters.AddWithValue("@AutomobilBoja", automobil.Boja);
                sqlCommand.Parameters.AddWithValue("@AutomobilTipMotoraId", automobil.TipMotora.Id);

                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
        }
    }
}