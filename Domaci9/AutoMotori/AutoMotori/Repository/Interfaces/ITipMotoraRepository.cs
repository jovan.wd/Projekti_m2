﻿using AutoMotori.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoMotori.Repository.Interfaces
{
    interface ITipMotoraRepository
    {
        IEnumerable<TipMotora> GetAll();
        TipMotora GetById(int id);
        bool Create(TipMotora tipMotora);
        void Update(TipMotora tipMotora);
        void Delete(int id);
    }
}
