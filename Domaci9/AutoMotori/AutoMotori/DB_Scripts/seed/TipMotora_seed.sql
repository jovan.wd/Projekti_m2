﻿delete from TipMotora

DBCC CHECKIDENT ('TipMotora', RESEED, 0)
GO

insert into TipMotora(TipMotoraNaziv)
values
	('Benzin'),
	('Dizel'),
	('TNG'),
	('Elektro')