﻿CREATE TABLE Automobil
(
	AutomobilId INT IDENTITY(1,1) PRIMARY KEY,
	AutomobilProizvodjac NVARCHAR(80) NULL, 
    AutomobilModel NVARCHAR(80) NULL, 
	AutomobilGodisete int,
	AutomobilZapremina int,
	AutomobilBoja NVARCHAR(10),
	AutomobilTipMotoraId int,
	FOREIGN KEY (AutomobilTipMotoraId) REFERENCES TipMotora(TipMotoraId)
)