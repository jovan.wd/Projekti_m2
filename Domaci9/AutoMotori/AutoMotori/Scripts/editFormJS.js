﻿window.onload = function () {
    var proizvodjac = document.querySelector("input[name='Automobil.Proizvodjac']");
    var model = document.querySelector("input[name='Automobil.Model']");
    var godiste = document.querySelector("input[name='Automobil.Godiste']");
    var zapremina = document.querySelector("input[name='Automobil.Zapremina']");
    var boja = document.querySelector("input[name='Automobil.Boja']");
    var tipid = document.querySelector("input[name='Automobil.Boja']");

    proizvodjac.onfocus = function () {
        let proizvodjacSpan = document.querySelector("#proizvodjacSpan");
        proizvodjacSpan.innerText = "";
    };

    model.onfocus = function () {
        let modelSpan = document.querySelector("#modelSpan");
        modelSpan.innerText = "";
    };

    godiste.onfocus = function () {
        let godisteSpan = document.querySelector("#godisteSpan");
        godisteSpan.innerText = "";
    };

    zapremina.onfocus = function () {
        let zapreminacSpan = document.querySelector("#zapreminaSpan");
        zapreminaSpan.innerText = "";
    };

    boja.onfocus = function () {
        let bojaSpan = document.querySelector("#bojaSpan");
        bojaSpan.innerText = "";
    };

};

function posalji(form) {
    var proizvodjac = form["Automobil.Proizvodjac"].value;
    var model = form["Automobil.Model"].value;
    var godiste = form["Automobil.Godiste"].value;
    var zapremina = form["Automobil.Zapremina"].value;
    var boja = form["Automobil.Boja"].value;
    var tipId = form["Automobil.TipMotora.Id"].value;

    if (!proizvodjac) {
        let proizvodjacSpan = document.querySelector("#proizvodjacSpan");
        proizvodjacSpan.innerText = "polje ne sme biti prazno";
    }
    if (!model) {
        let modelSpan = document.querySelector("#modelSpan");
        modelSpan.innerText = "polje ne sme biti prazno";
    }
    if (!godiste) {
        let godisteSpan = document.querySelector("#godisteSpan");
        godisteSpan.innerText = "polje ne sme biti prazno";
    }
    if (!zapremina) {
        let zapreminaSpan = document.querySelector("#zapreminaSpan");
        zapreminaSpan.innerText = "polje ne sme biti prazno";
    }
    if (!boja) {
        let bojaSpan = document.querySelector("#bojaSpan");
        bojaSpan.innerText = "polje ne sme biti prazno";
    }
    if (godiste < 1990) {
        let godisteSpan = document.querySelector("#godisteSpan");
        godisteSpan.innerText = "godiste mora biti izmedju 1990-2018";
    }
    if (godiste > 2018) {
        let godisteSpan = document.querySelector("#godisteSpan");
        godisteSpan.innerText = "godiste mora biti izmedju 1990-2018";
    }
    if (zapremina < 900) {
        let zapreminaSpan = document.querySelector("#zapreminaSpan");
        zapreminaSpan.innerText = "kubikaza mora biti izmedju 900 - 3000 ccm3";
    }
    if (zapremina > 3000) {
        let zapreminaSpan = document.querySelector("#zapreminaSpan");
        zapreminaSpan.innerText = "kubikaza mora biti izmedju 900 - 3000 ccm3";
    }

    console.log(`Proizvodjac: ${proizvodjac}, model: ${model}, godiste: ${godiste}, kubikaza: ${zapremina} ccm3, boja: ${boja}, tip motora: ${tipId} `);
    return false;
}