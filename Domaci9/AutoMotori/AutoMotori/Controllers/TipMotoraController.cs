﻿using AutoMotori.Models;
using AutoMotori.Repository;
using AutoMotori.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AutoMotori.Controllers
{
    public class TipMotoraController : Controller
    {
        private ITipMotoraRepository tipMotoraRepo = new TipMotoraRepository();

        // GET: TipMotora
        public ActionResult Index()
        {
            var tipoviMotora = tipMotoraRepo.GetAll();

            return View(tipoviMotora);
        }

        // GET: TipMotora/Details/5
        public ActionResult Details(int id)
        {
            var tipMotora = tipMotoraRepo.GetById(id);

            return View(tipMotora);
        }

        // GET: TipMotora/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipMotora/Create
        [HttpPost]
        public ActionResult Create(TipMotora tipMotora)
        {
            if (tipMotoraRepo.Create(tipMotora))
            {
                return RedirectToAction("Index");   // nakon uspesnog dodavanja daj listing svih proizvoda
            }
            return View();
        }

        // GET: TipMotora/Edit/5
        public ActionResult Edit(int id)
        {
            var tipMotora = tipMotoraRepo.GetById(id);

            return View(tipMotora);
        }

        // POST: TipMotora/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            var tipMotora = new TipMotora();

            try
            {
                UpdateModel(tipMotora);

                tipMotoraRepo.Update(tipMotora);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(tipMotora);
            }
        }

        // GET: TipMotora/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                tipMotoraRepo.Delete(id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }

            return RedirectToAction("Index");
        }
    }
}