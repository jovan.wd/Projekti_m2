﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace BookStore.Controllers
{
    public class BookStoreController : Controller
    {
        public static List<Book> knjige = new List<Book>();
        // GET: BookStore
        public ActionResult Index()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<html>");
            sb.Append("<head>");
            sb.Append("<style>");
            sb.Append("h1 {font-size: 40px;}");
            sb.Append(".hr-3 {height: 3px; background-color: grey;}");
            sb.Append(".right {text-align: right;}");
            sb.Append(".center {text-align: center;}");
            sb.Append("table {font-size :20px;}");
            sb.Append("</style>");
            sb.Append("</head>");
            sb.Append("<body>");
            sb.Append("<hr class='hr-3' />");
            sb.Append("<h1>Book Store - Home Page</h1>");
            sb.Append("<hr /><br/>");
            sb.Append("<form method='get' action='KreirajKnjigu'>");
            sb.Append("<table border='2'>");
            sb.Append("<tr>");
            sb.Append("<th class='center' colspan='2'>Add new book</th>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<th >Name of book:</th>");
            sb.Append("<td><input type='text' name='Name' required/></td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<th>Price of book:</th>");
            sb.Append("<td><input type='number' name='Price' required /></td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<th>Genre of book:</th>");
            sb.Append(
                "<td>" +
                "<input list='genre' name='Genre' required >" +
                "<datalist id='genre'>" +
                "<option value='Science'>" +
                "<option value='Comedy'>" +
                "<option value='Horror>'" +
                "</datalist>" +
                "</td>");
            sb.Append("</tr>");
            sb.Append("<tr><td class='right' colspan='2'><input type='submit' value='Add' /></td></tr>");
            sb.Append("</table>");
            sb.Append("<font size='4'><a href='List'>Show all books</a> | <a href='Deleted'>Show deleted books</a></font>");
            sb.Append("</form>");
            
            sb.Append("</body>");
            sb.Append("</html>");

            return Content(sb.ToString());
        }

        public ActionResult Deleted()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<html>");
            sb.Append("<head>");
            sb.Append("<style>");
            sb.Append("h1 {font-size: 40px;}");
            sb.Append(".hr-3 {height: 3px; background-color: grey;}");
            sb.Append(".right {text-align: right;}");
            sb.Append(".center {text-align: center;}");
            sb.Append("table {font-size :20px;}");
            sb.Append("</style>");
            sb.Append("</head>");
            sb.Append("<body>");
            sb.Append("<h1>Show Deleted Books</h1>");
            sb.Append("<hr class='hr-3' />");
            sb.Append("<table border='2'>");
            sb.Append("<tr><th>Id</th> <th>Name</th> <th>Price</th> <th>Genre</th></tr>");
            foreach (Book knjiga in knjige)
            {
                if(knjiga.Deleted==true)
                sb.Append($"<tr><td>{knjiga.Id}</td> <td>{knjiga.Name}</td> <td>{knjiga.Price}</td> <td>{knjiga.Genre}</td></tr>");
            }
            sb.Append("</table>");
            sb.Append("<font size='4'><a href='Index'>Link for HomePage</a></font>");
            sb.Append("</body>");
            sb.Append("</html>");
            return Content(sb.ToString());
        }

        [HttpGet]
        public ActionResult List()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<html>");
            sb.Append("<head>");
            sb.Append("<style>");
            sb.Append("h1 {font-size: 40px;}");
            sb.Append(".hr-3 {height: 3px; background-color: grey;}");
            sb.Append(".right {text-align: right;}");
            sb.Append(".center {text-align: center;}");
            sb.Append("table {font-size :20px;}");
            sb.Append("</style>");
            sb.Append("</head>");
            sb.Append("<body>");
            sb.Append("<h1>Show All Books</h1>");
            sb.Append("<hr class='hr-3' />");
            sb.Append("<table border='2'>");
            sb.Append("<tr><th>Id</th> <th>Name</th> <th>Price</th> <th>Genre</th></tr>");
            foreach (Book knjiga in knjige)
            {
                if (knjiga.Deleted == false)
                    sb.Append($"<tr><td>{knjiga.Id}</td> <td>{knjiga.Name}</td> <td>{knjiga.Price}</td> <td>{knjiga.Genre}</td><td><a href='Delete({knjiga.Id})'>delete</a></td></tr>");
            }
            sb.Append("</table>");
            sb.Append("<font size='4'><a href='Index'>Link for HomePage</a></font>");
            sb.Append("</body>");
            sb.Append("</html>");
            return Content(sb.ToString());
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            foreach (Book b in knjige)
            {
                if (b.Id == id)
                    b.Deleted = true;
            }
            return List();
        }
        
        [HttpGet]
        public ActionResult KreirajKnjigu(Book book)
        {
            BookStore books = new BookStore();
            books.Name = "Books Listing";

            book = new Book(book.Name, book.Price, book.Genre, book.Deleted=false);
            knjige.Add(book);
            
            return List();
        }

    }
}
