﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Controllers
{
    public class Book
    {
        private static int brojacID = 1;

        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string Genre { get; set; }
        public bool Deleted { get; set; }

        public Book()
        {
            Id = brojacID;
            Name = "";
            Price = 0;
            Genre = "";
            Deleted = false;
        }
        public Book(string name, double price, string genre, bool deleted = false)
        {
            Id = brojacID;
            brojacID++;
            Name = name;
            Price = price;
            Genre = genre;
            Deleted = deleted;
        }
    }
}