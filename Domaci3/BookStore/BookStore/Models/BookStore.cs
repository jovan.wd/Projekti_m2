﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Controllers
{
    public class BookStore
    {
        private static int brojacID=0;

        public int Id { get; set; }
        public string Name { get; set; }
        public List<Book> Books { get; set; }

        public BookStore()
        {
            Id = brojacID;
            brojacID++;
            Name = "";
            Books = new List<Book>();
        }
    }
}